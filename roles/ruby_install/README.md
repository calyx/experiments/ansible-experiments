Ruby Install
=========

Compile and install ruby from source

Requirements
------------

Debian 11+

Role Variables
--------------

By default, it installs ruby 3.2.2 in /opt/rubies/3.2.2.
To install in the normal location `/usr/local`, set `ruby_local_install` to `true`
Use `ruby_version` to change the ruby version.
To install as a regular user set `ruby_user` to any user besides root. The user must already exist. It will install rubies in `/home/rubyuser/.rubies/3.2.2`

Example Playbook
----------------

    - hosts: my-ruby-webapp
      roles:
         - { role: ruby_install, ruby_version: 3.1.4, ruby_user: 'webapp' }
