Role Name
=========

Installs ruby using rbenv

Requirements
------------

Debian 12+

Role Variables
--------------

``` yaml
rbenv_user: webapp  # User account to install rbenv
ruby_version: 3.2.2 # Version of ruby to install
```

Example Playbook
----------------

    - hosts: servers
      roles:
         - role: calyx.experiments.rbenv
		   rbenv_user: webapp
		   ruby_version: 3.2.2
