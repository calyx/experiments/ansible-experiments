import os


def podman_ports(nums):
    return list(map(lambda n: n if ":" in n else f"127.0.0.1:{n}:{n}", nums))


def podman_volume_path(volume, user="podman"):
    if ":" in volume:
        volume = volume[0 : volume.index(":")]

    return os.path.join(f"/home/{user}", volume)


def podman_volume_bindmount(volume, user="podman"):
    return podman_volume_path(volume, user=user) + ":" + os.path.join("/", volume)


def podman_volumes(volumes, user="podman"):
    return list(map(lambda v: podman_volume_bindmount(v, user=user), volumes))


class FilterModule(object):
    def filters(self):
        return {
            "podman_ports": podman_ports,
            "podman_volumes": podman_volumes,
            "podman_volume_bindmount": podman_volume_bindmount,
            "podman_volume_path": podman_volume_path,
        }
